# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM_NASM"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM_NASM
  "/home/user/Desktop/CodaLab/src/asm.asm" "/home/user/Desktop/CodaLab/build/CMakeFiles/CodeLab.dir/asm.asm.o"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/user/Desktop/CodaLab/src/main.cpp" "/home/user/Desktop/CodaLab/build/CMakeFiles/CodeLab.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
