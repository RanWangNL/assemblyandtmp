#include <iostream>

using namespace std;

extern "C" int GetValueFromASM(int);
int main() {
    cout << "Hello, World!" << endl;
    cout << GetValueFromASM(2) << endl;
    return 0;
}